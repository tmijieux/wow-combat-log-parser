from ..parsers.prefix_parsers import *
from ..parsers.suffix_parsers import *


class EventHandlers:
    def __init__(self, version):
        self.prefixes = {
            'SWING': SwingParser(version),
            'SPELL_BUILDING': SpellParser(version),
            'SPELL_PERIODIC': SpellParser(version),
            'SPELL': SpellParser(version),
            'RANGE': SpellParser(version),
            'ENVIRONMENTAL': EnvironmentParser(version)
        }
        self.suffixes = {
            '_DAMAGE': DamageParser(version),
            '_MISSED': MissParser(version),
            '_HEAL': HealParser(version),
            '_ENERGIZE': EnergizeParser(version),
            '_DRAIN': DrainLeechParser(version),
            '_LEECH': DrainLeechParser(version),
            '_INTERRUPT': InterruptParser(version),
            '_DISPEL': DispellParser(version),
            '_DISPEL_FAILED': DispellParser(version),
            '_STOLEN': StolenParser(version),
            '_EXTRA_ATTACKS': ExtraAttacksParser(version),
            '_AURA_APPLIED': AuraParser(version),
            '_AURA_REMOVED': AuraParser(version),
            '_AURA_APPLIED_DOSE': AuraParser(version),
            '_AURA_REMOVED_DOSE': AuraParser(version),
            '_AURA_REFRESH': AuraParser(version),
            '_AURA_BROKEN': AuraParser(version),
            '_AURA_BROKEN_SPELL': AuraBrokenParser(version),
            '_CAST_START': CastParser(version),
            '_CAST_SUCCESS': CastParser(version),
            '_CAST_FAILED': CastParser(version),
            '_INSTAKILL': DummySuffixParser(version),
            '_DURABILITY_DAMAGE': DummySuffixParser(version),
            '_DURABILITY_DAMAGE_ALL': DummySuffixParser(version),
            '_CREATE': CreateSumResParser(version),
            '_SUMMON': CreateSumResParser(version),
            '_RESURRECT': CreateSumResParser(version)
        }
        self.other_events = {
            'DAMAGE_SHIELD': (SpellParser(version), DamageParser(version)),
            'DAMAGE_SPLIT': (SpellParser(version), DamageParser(version)),
            'DAMAGE_SHIELD_MISSED': (SpellParser(version), MissParser(version)),
            'ENCHANT_APPLIED': (EnchantmentParser(), DummySuffixParser()),
            'ENCHANT_REMOVED': (EnchantmentParser(), DummySuffixParser()),
            'PARTY_KILL': (DummyPrefixParser(version), DummySuffixParser(version)),
            'UNIT_DIED': (DummyPrefixParser(version), DummySuffixParser(version)),
            'UNIT_DESTROYED': (DummyPrefixParser(version), DummySuffixParser(version))
        }
