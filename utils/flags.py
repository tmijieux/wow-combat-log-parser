def get_spell_school(school_flags):
    schools = {
        0x1: "Physical",
        0x2: "Holy",
        0x4: "Fire",
        0x8: "Nature",
        0x10: "Frost",
        0x20: "Shadow",
        0x40: "Arcane",
    }

    school_list = list()
    sf = int(school_flags, 0) if isinstance(school_flags, str) else school_flags

    for (flag, school) in schools.items():
        if sf & flag > 0:
            school_list.append(school)

    if "Fire" in school_list and "Frost" in school_list:
        school_list.remove("Fire")
        school_list.remove("Frost")
        school_list.append("Firefrost")

    return school_list


def get_unit_type(unit_flag):
    unit_type_flags = {
        0x00004000: 'TYPE_OBJECT',
        0x00002000: 'TYPE_GUARDIAN',
        0x00001000: 'TYPE_PET',
        0x00000800: 'TYPE_NPC',
        0x00000400: 'TYPE_PLAYER',
        0x00000200: 'CONTROL_NPC',
        0x00000100: 'CONTROL_PLAYER',
        0x00000040: 'REACTION_HOSTILE',
        0x00000020: 'REACTION_NEUTRAL',
        0x00000010: 'REACTION_FRIENDLY',
        0x00000008: 'AFFILIATION_OUTSIDER',
        0x00000004: 'AFFILIATION_RAID',
        0x00000002: 'AFFILIATION_PARTY',
        0x00000001: 'AFFILIATION_MINE',
        0x08000000: 'RAIDTARGET8',
        0x04000000: 'RAIDTARGET7',
        0x02000000: 'RAIDTARGET6',
        0x01000000: 'RAIDTARGET5',
        0x00800000: 'RAIDTARGET4',
        0x00400000: 'RAIDTARGET3',
        0x00200000: 'RAIDTARGET2',
        0x00100000: 'RAIDTARGET1',
        0x00080000: 'MAINASSIST',
        0x00040000: 'MAINTANK',
        0x00020000: 'FOCUS',
        0x00010000: 'TARGET'
    }

    type_list = list()
    uf = int(unit_flag, 0) if isinstance(unit_flag, str) else unit_flag

    if uf != 0:
        for (flag, unit_type) in unit_type_flags.items():
            if uf & flag > 0:
                type_list.append(unit_type)

    return type_list


def get_power_type(value):
    power_types = dict()
    power_types[-2] = "health"
    power_types[0] = "mana"
    power_types[1] = "rage"
    power_types[2] = "focus"
    power_types[3] = "energy"
    power_types[4] = "pet happiness"
    power_types[5] = "runes"
    power_types[6] = "runic power"

    return power_types[value]
